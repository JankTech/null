package null_test

import (
	"encoding/json"
	"errors"
	"testing"

	"gitlab.com/JankTech/null"
)

type address struct {
	L1 string            `json:"line1"`
	L2 null.Null[string] `json:"line2"`
}

// Unmarshal a null value into Null[string]
func TestUnmarshalANullValue(t *testing.T) {
	t.Parallel()
	var n null.Null[string]
	if err := json.Unmarshal([]byte("null"), &n); err != nil {
		panic(err)
	}
	if n.Valid() {
		t.Errorf("unexpected validity of null value, expected invalid, got valid")
	}
	_, err := n.Value()
	if !errors.Is(err, null.ErrNullValue) {
		t.Errorf("unexpected error value trying to get null value, got %T, expected %T", err, null.ErrNullValue)
	}
}

// Unmarshal a non-null value into Null[string]
func TestUnmarshalNonNullValue(t *testing.T) {
	t.Parallel()
	var n null.Null[string]
	if err := json.Unmarshal([]byte("\"world\""), &n); err != nil {
		panic(err)
	}

	v, err := n.Value()
	if err != nil {
		t.Errorf("unexpected error while getting value, got %T, expected nil", err)
	}
	expected := "world"
	if v != expected {
		t.Errorf("unexpected value, got %s, expected %s", v, expected)
	}
}

func TestMarshalANullString(t *testing.T) {
	t.Parallel()
	n := null.NullValue[string]()
	marshaledNull, err := json.Marshal(n)
	if err != nil {
		panic(err)
	}
	if string(marshaledNull) != "null" {
		t.Error("expected null json output")
	}
}

func TestMarshalNonNullString(t *testing.T) {
	t.Parallel()
	n := null.NewNull("hello")
	marshaledNonNull, err := json.Marshal(n)
	if err != nil {
		panic(err)
	}
	if string(marshaledNonNull) != "\"hello\"" {
		t.Error("expected \"hello\" json output")
	}
}

// Marshal a struct containing a null
func TestMarshalAStructContainingANull(t *testing.T) {
	t.Parallel()
	a := address{
		L1: "my house",
		L2: null.NullValue[string](),
	}

	j, err := json.Marshal(a)
	if err != nil {
		panic(err)
	}

	expected := "{\"line1\":\"my house\",\"line2\":null}"
	actual := string(j)
	if actual != expected {
		t.Errorf("unexpected json output, got %s, expected %s", actual, expected)
	}
}

// Unmarshal a JSON object containing a null
func TestUnmarshalAnObjectContainingANull(t *testing.T) {
	t.Parallel()
	j := `{"line1": "my house", "line2": null}`
	a := address{}
	err := json.Unmarshal([]byte(j), &a)
	if err != nil {
		panic(err)
	}

	expected := "my house"
	if a.L1 != expected {
		t.Errorf("unexpected value of L1 after marshalling, got %s, expected %s", a.L1, expected)
	}
	if a.L2.Valid() {
		t.Error("unexpected validity of L2 after marshalling, got valid, expected invalid", a.L1)
	}
	_, err = a.L2.Value()
	if !errors.Is(err, null.ErrNullValue) {
		t.Errorf("unexpected error value trying to get null value, got %T, expected %T", err, null.ErrNullValue)
	}
}

// Unmarshal an object containing a value into a Null[string]
func TestUnmarshalAnObjectContainingAValueIntoNull(t *testing.T) {
	j := `{"line1": "my house", "line2": "some road"}`
	a := address{}
	err := json.Unmarshal([]byte(j), &a)
	if err != nil {
		panic(err)
	}

	expected := "my house"
	if a.L1 != expected {
		t.Errorf("unexpected value of L1 after marshalling, got %s, expected %s", a.L1, expected)
	}
	if !a.L2.Valid() {
		t.Error("unexpected validity of L2 after marshalling, got invalid, expected valid", a.L1)
	}
	v, err := a.L2.Value()
	if err != nil {
		t.Errorf("unexpected error value trying to get null value, got %T, expected nil", err)
	}

	expected = "some road"
	if v != expected {
		t.Errorf("unexpected value of L2, got %s, expected %s", v, expected)
	}
}

// Unmarshal an object missing a value into a Null[string]
func TestUnmarshalAnObjectMissingAValueIntoNull(t *testing.T) {
	j := `{"line1": "my house"}`
	a := address{}
	err := json.Unmarshal([]byte(j), &a)
	if err != nil {
		panic(err)
	}

	expected := "my house"
	if a.L1 != expected {
		t.Errorf("unexpected value of L1 after marshalling, got %s, expected %s", a.L1, expected)
	}
	if a.L2.Valid() {
		t.Error("unexpected validity of L2 after marshalling, got valid, expected invalid", a.L1)
	}
	_, err = a.L2.Value()
	if !errors.Is(err, null.ErrNullValue) {
		t.Errorf("unexpected error value trying to get null value, got %T, expected %T", err, null.ErrNullValue)
	}
}
