# Nullable Types for Go

This project provides a generic implementation of nullable types in Go, offering a convenient way to work with JSON data that may contain null values. Utilizing Go's generics, the `Null[T any]` type can wrap any other type to represent a value that can be null or non-null, seamlessly integrating with the standard encoding/json package for marshaling and unmarshaling.

## Features

- Generic implementation compatible with any type.
- Custom JSON marshaling and unmarshaling that correctly handles null values.

## Installation

To use this package, you need to have Go installed on your system (version 1.18 or higher, due to the use of generics). Install the package by running:

```sh
go get gitlab.com/JankTech/null
```

## Usage

Here's a quick example to show how you can use the `Null[T any]` type:

```go
package main

import (
    "encoding/json"
    "fmt"
    "errors"
    "gitlab.com/JankTech/null"
)

func main() {
    // Handling with Valid()
    var ns null.Null[string]
    json.Unmarshal([]byte("null"), &ns)
    if ns.Valid() {
      // handle value is not null case
    }
    // handle value is null case

    // ...

    // Handling with Value()
    json.Unmarshal([]byte("\"hello\""), &ns)
    v, err := ns.Value()
    if errors.Is(err, null.ErrNullValue) {
      // handle value is null case
    }
    // handle value is not null case
}
```

## Documentation

[![Go Reference](https://pkg.go.dev/badge/gitlab.com/JankTech/null.svg)](https://pkg.go.dev/gitlab.com/JankTech/null)

Full `go doc` style documentation for the package can be viewed online without
installing this package by using the GoDoc site here: 
http://pkg.go.dev/gitlab.com/JankTech/null

## Contributing

Contributions are welcome from everyone. Here are some ways you can contribute:

- Reporting bugs
- Suggesting enhancements
- Submitting pull requests

## License

This project is open source and available under the MIT License. See the LICENSE file for more info.
