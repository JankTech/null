package null

import (
	"encoding/json"
	"errors"
)

var ErrNullValue = errors.New("null value")

// Null represents a nullable type for any type T.
type Null[T any] struct {
	value T
	valid bool // Valid is true if Value is not null
}

// NewNull creates a new Null instance with a given value.
func NewNull[T any](value T) Null[T] {
	return Null[T]{value: value, valid: true}
}

// NullValue creates a new Null instance representing a null value.
func NullValue[T any]() Null[T] {
	return Null[T]{valid: false}
}

// Value returns the value of a valid Null[T] or ErrInvalidValue if invalid.
func (n Null[T]) Value() (T, error) {
	if !n.valid {
		return n.value, ErrNullValue
	}
	return n.value, nil
}

// Valid returns true if the the Null[T] is valid, and false if not.
func (n Null[T]) Valid() bool {
	return n.valid
}

// MarshalJSON implements the json.Marshaler interface for Null[T].
func (n Null[T]) MarshalJSON() ([]byte, error) {
	if !n.valid {
		return []byte("null"), nil
	}
	return json.Marshal(n.value)
}

// UnmarshalJSON implements the json.Unmarshaler interface for Null[T].
func (n *Null[T]) UnmarshalJSON(data []byte) error {
	if string(data) == "null" {
		n.valid = false
		return nil
	}
	n.valid = true
	return json.Unmarshal(data, &n.value)
}
